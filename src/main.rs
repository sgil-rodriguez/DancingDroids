#[derive(Copy, Clone)]
pub struct Robot {
    id: char,
    x: u8,
    y: u8,
    orientation: char,
}

mod _orders {
    use super::*;
    // La fonction forward agit en fonction de l'orientation du robot, vérifie qu'il ne se cogne
    // pas contre un mur ni contre un autre robot avant de changer la position du robot et
    // affiche un message en cas de collision
    pub fn forward(mut droid1: Robot, droid2: Robot, x_max: u8, y_max: u8) -> Robot {
        match droid1.orientation {
            'N' => if droid1.y == y_max {
                println!("le droide {} se tape la tête contre le mur au Nord en x = {} y = {}, il est un peu teubé, ignore-le...", droid1.id, droid1.x, droid1.y)
            } else if droid1.x == droid2.x && droid1.y + 1 == droid2.y {
                println!("le droide {} collisionne avec le droide {} en x = {} y = {}, il n'a pas respecté les règles de distancement social :/", droid1.id, droid2.id, droid2.x, droid2.y)
            } else {
                droid1.y = droid1.y + 1
            },
            'E' => if droid1.x == x_max {
                println!("le droide {} se tape la tête contre le mur à l'Est en x = {} y = {}, il a perdu ses lentilles, aidez-le!", droid1.id, droid1.x, droid1.y)
            } else if droid1.y == droid2.y && droid1.x + 1 == droid2.x {
                println!("le droide {} collisionne avec le droide {} en x = {} y = {}, vous avez fait exprès de leur faire choquer entre eux, pas vrai? c'est pas sympa de votre part", droid1.id, droid2.id, droid2.x, droid2.y)
            } else {
                droid1.x = droid1.x + 1
            },
            'S' => if droid1.y == 0 {
                println!("le droide {} se tape la tête contre le mur à l'Ouest en x = {} y = {}, il est clairement trop bourré", droid1.id, droid1.x, droid1.y)
            } else if droid1.x == droid2.x && droid1.y - 1 == droid2.y {
                println!("le droide {} collisionne avec le droide {} en x = {} y = {}, tiens, on dirait des parisiens dans le métro!", droid1.id, droid2.id, droid2.x, droid2.y)
            } else {
                droid1.y = droid1.y - 1
            },
            'W' => if droid1.x == 0 {
                println!("le droide {} se tape la tête contre le mur au Sud en x = {} y = {}, le mur est toujours bien, c'est sympa de demander", droid1.id, droid1.x, droid1.y)
            } else if droid1.y == droid2.y && droid1.x - 1 == droid2.x {
                println!("le droide {} collisionne avec le droide {} en x = {} y = {}, mais saviez vous que les atomes se repoussent entre eux à cause des électrons de ses couches extérieures? cela veut dire qu'il est impossible que deux atomes arrivent à se toucher, ce qui veut à la fois dire que lors de la collision, les atomes des deux droides n'ont rien touché, et donc que les droides n'ont jamais collisionné! :D", droid1.id, droid2.id, droid2.x, droid2.y)
            } else {
                droid1.x = droid1.x - 1
            },
            _ => (),
        };
        droid1
    }
    // Les fonctions left et right changent l'orientation du robot, vous l'aviez peut etre deviné
    // mais on sait jamais...
    pub fn left(mut droid: Robot) -> Robot{
        match droid.orientation {
            'N' => droid.orientation = 'W',
            'E' => droid.orientation = 'N',
            'S' => droid.orientation = 'E',
            'W' => droid.orientation = 'S',
            _ => (),
        };
        droid
    }
    pub fn right(mut droid: Robot) -> Robot{
        match droid.orientation {
            'N' => droid.orientation = 'E',
            'E' => droid.orientation = 'S',
            'S' => droid.orientation = 'W',
            'W' => droid.orientation = 'N',
            _ => (),
        };
        droid
    }
}

// La fonction _move lance la fonction correspondante a l'ordre que reçoit le robot ce tour-ci
// ou vous... informe gentiment si vous vous êtes trompé en introduisant vos instructions
fn _move(mut droid1: Robot, droid2: Robot, order: char, x_max: u8, y_max: u8) -> Robot {
    match order {
        'F' => droid1 = _orders::forward(droid1, droid2, x_max, y_max),
        'L' => droid1 = _orders::left(droid1),
        'R' => droid1 = _orders::right(droid1),
        _ => println!("ta fouarai en aicryvan T zinstrukssion, re tourne a lèkoll, T atar D")
    };
    droid1
}

fn main() {
    // Vous povez changer la valeur des deux varible ci-dessous pour changer la taille du terrain
    // faites gaffe, si vous voulez mettre une taille supérieure a 255 vous devrez changer le
    // type des variables x_max et y_max dans les fonctions qui les utilisent
    let x_max = 5;
    let y_max = 5;
    // Si vous voulez changer les ordres des robots, vous povez changer le contenu entre guillemets
    // par des F, des R et des L... ou par d'autres lettres, mais dans ce cas il s'affichera une
    // vérité que vous ne voulez peut être pas entendre ¯\_(ツ)_/¯
    let s1 = "F".to_string();
    let s2 = "F".to_string();
    let ord1: Vec<_> = s1.chars().collect();
    let ord2: Vec<_> = s2.chars().collect();
    // Si vous voulez changer la position ou l'orientation initiales ou l'id des robots vous povez
    // le faire en changeant les valeurs ci-dessous, malheureusement je n'ai pas trop compris ce 
    // que sont les "paramètres lifetime", du coup l'id ne peut etre qu'un seul caractère, mais 
    // vous pouvez chosir n'importe lequel, faites vous plaisir :D
    let mut r1 = Robot {
        id: '1',
        x: 0,
        y: 0,
        orientation: 'N',
    };
    let mut r2 = Robot {
        id: '2',
        x: 5,
        y: 5,
        orientation: 'S',
    };
    println!("Taille du terrain: {} x {}", x_max + 1, y_max + 1);
    println!("Le droide {} commence en position x = {}, y = {} et orienté vers {}", r1.id, r1.x, r1.y, r1.orientation);
    println!("Le droide {} commence en position x = {}, y = {} et orienté vers {}", r2.id, r2.x, r2.y, r2.orientation);
    // Cette suite de ifs et de fors fais en sorte que les tours des robots s'alternent
    if ord1.len() >= ord2.len() {
        for i in 0..ord2.len() {
            r1 = _move(r1, r2, ord1[i], x_max, y_max);
            r2 = _move(r2, r1, ord2[i], x_max, y_max);
        }
        for i in ord2.len()..ord1.len() {
            r1 = _move(r1, r2, ord1[i], x_max, y_max);
        }
    } else {
        for i in 0..ord1.len() {
            r1 = _move(r1, r2, ord1[i], x_max, y_max);
            r2 = _move(r2, r1, ord2[i], x_max, y_max);
        }
        for i in ord1.len()..ord2.len() {
            r2 = _move(r2, r1, ord2[i], x_max, y_max);
        }
    };
    println!("Le droide {} finit en position x = {}, y = {} et orienté vers {}", r1.id, r1.x, r1.y, r1.orientation);
    println!("Le droide {} finit en position x = {}, y = {} et orienté vers {}", r2.id, r2.x, r2.y, r2.orientation);
}
